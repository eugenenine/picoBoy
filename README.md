This originally started as a simple Arduino based GPS logger and display but as I started wanting to add additional features quickly exceeded the Arduino's capacity. I had bought a couple Raspberry Pi Pico's when they first becaeme available and hadn't done any projects with them yet. Knowing a little C but even less python and learning that the pico is supported by the Arduino IDE I went that route. While many may not prefer the Arduino IDE I was suprised at how easy it was to simply switch over to the Pico. I happened to stumble arcoss Adafruit's Pip-Boy 2040 https://learn.adafruit.com/pip-boy-2040/overview whiule looking at some of their display docuentation and thought I'd add some of its features as a learning exercise. I seached for more images for inspiration and downloaded theirs and used parts of some of the images and decided to call mine the pico-Boy.

**Design goals:**
* This is more of a learning platform for me, I haven't done much with programming or embedded since college and wanted to refresh my skills a bit.
* By the time I used the SoftwareSerial, SD and display libraries on the Arduino it didn't have enought resources free to use any of the available GPS libraries so I had to write my own minimal parser for the data and write it very basec. The pico has so much more resources that I can just keep adding features.
* I like the main menu switch of the Pip-Boy (I'm guessing thats why its called, haven't played the game), it can be set by feel without needing to look down to see where you are currently. So I implemented their five main menus.
* I was able to add a gpx "header" and write a gpx file so there is no need to convert after. Still need to write a "footer" to close out the xml properly, will need to implement a close or shutdown.
* Was able to add a RTC so the time and date can be read and gpx file named before the gps module gets a fix.
* Made a simple preferences file so the menu colors, time zone, etc can be set.
* Added a simple inventory where .csv files can be read, displayed and used.
* Some soft of mapping
* Sort of an OS, the pip boy runs https://fallout.fandom.com/wiki/Pip-OS_v7.1.0.8


**Current architecture:**

setup() will initialize the rtc, display, sd, gps then reads the preferences inventory and map files if present.

loop() runs a state machine where the state of the main menu switch, current sub menu, current date, current time, current location and if and when any of those change updates the display, gpx log, etc.

drawmenu() draws the main menu as well as the SD status indacator in the upper left. If the SD is present and sucessfully initialized it will show a small icon indicating so. Each write flashes an arrow. Drew a simple tab around the active menu.

sumMenu() displays the sub menu and changes it as the rotary encoder is turned. I noticed recently that the original pipboy appears to scroll the menu if there are too many for the display, I need to implement that still.

each time the date or time changes the display at the bottom will update

STAT
I thought about adding a heart rate sensor and displaying the heart rate on the status menu so I used the non filled in pipboy to leave an open space for the number to be displayed

INV
Inventory csv is read and displayed, created a simple list of one pistol with four 12 round magazines. put the pistol icon by the hand and totaled up the ammo by it.

DATA
is just displaying the gps data at the monent and doesn't yet refresh.

MAP 
working on reading a gpx file, starting small with just a few waypoints to see if I can plot them.

RADIO
not sure what to put here yet and haven't enabled it in the selector.

On the SD card are 
prefs
inv
map
.gpx log files


**Work log:**
2024-11-15:
Moved the pinouts to a header file
Moved the sd card and rtc to separate headers
Little bit of code cleanup
Added a version

2023-05-07:
.gpx log file name to use two digit month and day
Added itemNum to inventory so ammo can be matched to weapon

2023-05-02:
Work on plotting gpx waypoints on map

2023-04-30:
Worked on basic cli commands cd,dir,makedir and a simple prompt and command line interface

2023-04-25:
Worked on getInv() to make it use a variable
fixed submenu item wrapping around

2023-04-24:
worked on waypoint from gpx
started on submenu selection in inv

2023-04-22:
figured out gpx import problem, seems to be long lines, check doesn't appear to be working. For now simplified gpx file and created waypoint type to start import

2023-04-19:
hour display if less than timezoneoffset wrapped, had t check for and adjust
tried adjusting day, isn't working

2023-04-18:
re-wrote preferences using loop to dynamically get the size which will be same code for reading inventory.
Reading time preferences form prefs
bug :sd detection seems to be not working, need to check for errors opening files and check card status if failed. need to detect if inserted afterward. weapon inv seems to have 5 even if no ssd card, where did 5 come from, NUMBER_OF_STRING, need to count first
 
2023-04-17:
local variable `timeZoneHour` in `displayTime` to adjust for time zone. Need to adjust date in `displayDate` if hour goes past midnight still.
timezone needs pulled from prefs still
worked on the newGetPrefs a little. want to use the same code/function as getInv just using = instead of , for the delimiter.

2023-04-13:
dir working to serial.print
cd works now for / if / is root, need to do for multi level.
updated pistol icon and ammo count
sd corrupted, had to re-create and made backup under project

2023-04-12:
added ammo to inv sub menu
worked on replacing the inventory csv file name with a loop so csv file name is generated from menu list

	for (int is = 0; is < iSubMenuNum; is++) {
	    int str_len = iSubMenu[is].length() + 4;  // Length (with one extra character for the null terminator)
	    char char_array[str_len]; // Prepare the character array (the buffer) 
	    iSubMenu[is].toCharArray(char_array, str_len); // Copy it over 
	    char_array[str_len-4] = '.';
	    char_array[str_len-3] = 'c';
	    char_array[str_len-2] = 's';
	    char_array[str_len-1] = 'v';
	    char_array[str_len] = '\0';
	    Serial.println(char_array);
    }
 
created a cd function to allow changing to a working directory on the sc card to get a directory listing in order to list all fles in a directory. 

2023-04-11:
read weapons.csv and display
added pistol icon and ammo count
 
2023-03-14:
* microsd card reader
* Heart rate/vo2 sensor
* accelermeter/movement sensor
* gps
* display
* menu knob
* power mgmt, shut down modules like GPS if desired

2023-03-15:
Using Ardunio's RP2040_SD Library

> In file included from /home/eugene/Projects/Electronics/Pico/SD_Read_example/SD_Read_example.ino:69:0:
> /root/Arduino/libraries/RP2040_SD/src/RP2040_SD.h:42:4: warning: #warning Using Arduino-mbed core and architecture for RP2040 [-Wcpp]
>    #warning Using Arduino-mbed core and architecture for RP2040
>     ^~~~~~~
> Sketch uses 101157 bytes (4%) of program storage space. Maximum is 2097152 bytes.
> Global variables use 43736 bytes (16%) of dynamic memory, leaving 226600 bytes for local variables. Maximum is 270336 bytes.

2023-03-18:

waveshare example
replace all printf with Serial.print
all /* */  with //

main.c calls LCD_2in_test(); from LCD_Test.h and LCD_Test.c

LCD_2in_test
- DEV_Delay_ms
- DEV_Module_Init
- - stdio_init_all()
- - spi_init
- - - spi_reset
- - - spi_unreset
- - gpio_set_function
- - DEV_GPIO_Init
- - PWM config
- - I2C config


2023-03-21
convert stat.bmp  -resize 50%  half_stat.bmp
http://javl.github.io/image2cpp/
https://learn.adafruit.com/adafruit-gfx-graphics-library/graphics-primitives