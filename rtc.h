char     TimeZonePosNeg   = '-';
int      TimeZoneOffset   = 4;
bool     TwelveHourTime   = 1;
uint16_t currentYear      = 1970;
uint16_t currentMonth     = 1;
uint16_t currentDay       = 1;
uint8_t  currentDayOfWeek = 0;
uint16_t currentHour      = 00;
uint16_t currentMinute    = 00;
uint16_t currentSecond    = 00;
static const char daysOfTheWeek[7][10] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
static const char monthOfTheYear[12][4] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov",  "Dec"};
RTC_DS1307 rtc;
  
extern Adafruit_ST7735 tft;

void setuprtc() {
    if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    tft.println("Couldn't find RTC");
  } else {
    Serial.println("RTC Initialized");
    tft.println("RTC Initialized");
  }

  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running, let's set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
}
