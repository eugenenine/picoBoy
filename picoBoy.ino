#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735
#include <SD.h>
#include "pinout.h"          // moved the pinouts here 2024-11-25
#include "Images.h"
#include "RTClib.h"
#include "rtc.h"             // moved all the clock stuff here 2024-11-25
#include "sd.h"              // moved all the sd card stuff here 2024-11-25
#include <TinyGPSPlus.h>

String   version          = "1.01";
#define  menu_Y_Origin   1 // different fonts have different origins, adjust position as necessary
#define  menuHeight      9 // different font size might need the menu line higher or lower
uint16_t menuColor        = 0x07E0; // default if no prefs
uint16_t menuDimColor     = 0x0400; // default if no prefs
char     previousMenu     = ' ';
char     currentMenu      = 's';
int      previousSubMenu  = -1;
int      currentSubMenu   = 0;
int      menuOne;
int      menuTwo;
double   currentLatitude  = 0;
double   currentLongitude = 0;
double   maxLat;
double   minLat;
double   maxLon;
double   minLon;
String   gpxfileName;
int      maxSubMenu       = 0;
#define  sSubMenuNum        3
#define  iSubMenuNum        4
#define  dSubMenuNum        3
String   sSubMenu[sSubMenuNum] = { "Status", "Special", "Perks" };
String   iSubMenu[iSubMenuNum] = { "Weapons", "Apparel", "Aid", "Misc"};// , "Ammo" have to work on display first
String   dSubMenu[dSubMenuNum] = { "Quests", "Workshops", "Stats" };
bool     subMenuSelected    = 0;
int      currentSubMenuItem = 0;
#define  CMDBUFFER_SIZE 100
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

TinyGPSPlus gps;

// inventory item number, item and quantity
typedef struct {
  int  itemNum;
  char item [100];
  int  quantity;
} inv_type;

inv_type invWeapon[10];
inv_type invApparel[10];
inv_type invAid[10];
inv_type invMisc[10];
inv_type invAmmo[10];
int currentWeapon = 0;
int numWeapons    = 0;
int ammoTotal     = 0;

// this holds the waypoints, latitude, longitude and waypint name
typedef struct {
  double lat;
  double lon;
  String name;
} wpt_type;

wpt_type waypoints[10];

#define MAX_STRING_SIZE 100

char cwd[100]      = "/"; // initial curent working directory
char inBuffer[100] = "\0"; // initial curent working directory

int val = 0;  // variable to store the value read
  
void setup() {

  Serial.begin(9600);             // debug
  Serial1.begin(9600);            // GPS

  tft.initR(INITR_BLACKTAB);      // Init ST7735S chip, black tab 1.8" TFT screen

  tft.fillScreen(0x328A);         // RBG565 Dark Grey
  tft.setRotation(1);
  pinMode(switch1,     INPUT_PULLUP); // main menu switch1
  pinMode(switch2,     INPUT_PULLUP); // main menu switch2
  pinMode(encoderApin, INPUT_PULLUP);
  pinMode(encoderBpin, INPUT_PULLUP);
  pinMode(encoderSw,   INPUT_PULLUP);

  tft.fillScreen(0xD69A);         // RBG565 Light Grey
  delay(1000);
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println("boot version " + version);
  tft.fillScreen(ST77XX_BLACK);
  tft.println("boot version " + version);

  setuprtc();
  setupSD();

    getPrefs();
    getMap();

    numWeapons = getInv("Inv/Weapons.csv");
    Serial.println(numWeapons);
    getInv("Inv/ammo.csv");

    for (int j = 0; j < numWeapons; j++) {
    ammoTotal += invWeapon[j].quantity;
    }
  prompt();
  delay(1000);
}

void loop() {

  val = analogRead(ADC2);
  //Serial.println(val);  
  static char cliBuffer[CMDBUFFER_SIZE] = "";
  char c;
  if (Serial.available() > 0) {
    c = processCharInput(cliBuffer, Serial.read());
    Serial.print(c);
    if (c == '\n') 
    { 
      char command[20]="", parameter1[20]="";
      sscanf( cliBuffer, "%s %s ",command, parameter1 );
      if (strcmp("cd"     , command) == 0) cd(parameter1);
      if (strcmp("dir"    , command) == 0) dir(parameter1);
      if (strcmp("date"   , command) == 0) date(parameter1);
      if (strcmp("echo"   , command) == 0) echo(parameter1);
      if (strcmp("makedir", command) == 0) makedir(parameter1);
      // need to do better than a bunch of if's, like https://forum.arduino.cc/t/best-method-for-comparing-multiple-character-arrays/306288/3 maybe
            
      cliBuffer[0] =  0;
      command[0]   =  '\0';
      parameter1[0] = '\0'; 
      prompt();
    }
  }

  //read the main menu selector
  menuOne = digitalRead(switch1);
  menuTwo = digitalRead(switch2);
  if (menuTwo == LOW) {
    if (menuOne == LOW) {
      currentMenu  = 'm';
    }     else {
      currentMenu  = 'd';
    }
  } else {
    if (menuOne == LOW) {
      currentMenu  = 'i';
    }     else {
      currentMenu  = 's';
    }
  }

  // read the encoder
  int8_t state = 0;
  if (rotaryEncoder(state)) {
    Serial.println("- SWITCH -");
    // these ifs needs cleaned up
    if ((currentMenu == 'i') && (currentSubMenu == 0)) {
      subMenuSelected = 1;
      subMenu(4);
    }
    if ((currentMenu == 's') && (currentSubMenu == 0)) {
      subMenuSelected = 1;
      currentWeapon = currentSubMenuItem;
      displayWeapon();
    }
  }
  if (state == -1) {
    //Serial.println("<-- ");
    if (subMenuSelected == 0) currentSubMenu--; 
    else {
      currentSubMenuItem--;
      if (currentSubMenuItem > numWeapons) currentSubMenuItem = 0;
      if (currentSubMenuItem < 0) { currentSubMenuItem = 0; subMenuSelected = 0; }
      subMenu(4);
      if ((currentMenu == 's') && (currentSubMenu == 0)) {
        currentWeapon = currentSubMenuItem;
        displayWeapon();
        }
      }
    //Serial.println(currentSubMenu);
    //Serial.println(subMenuSelected);
    //Serial.println(currentSubMenuItem);
  }
  if (state == 1) {
    //Serial.println(" -->");
    if (subMenuSelected == 0) currentSubMenu++; 
    else {
      currentSubMenuItem++;
      if (currentSubMenuItem > numWeapons) currentSubMenuItem = 0;
      if (currentSubMenuItem < 0) { currentSubMenuItem = 0; subMenuSelected = 0; }
      subMenu(4);
      if ((currentMenu == 's') && (currentSubMenu == 0)) {
        currentWeapon = currentSubMenuItem;
        displayWeapon();
        }
    }
    //Serial.println(currentSubMenu);
    //Serial.println(subMenuSelected);
    //Serial.println(currentSubMenuItem);
  }

  if (currentSubMenu > maxSubMenu - 1) currentSubMenu = 0;
  if (currentSubMenu < 0) currentSubMenu = maxSubMenu - 1;

  // if top menu has changed draw it
  if (currentMenu != previousMenu) {
    currentSubMenu = 0; // reset the submenu to 0 for consistency and so the value isn't too high for the shorter sub menus
    drawmenu(currentMenu);
    previousMenu = currentMenu;
  }

  // if submenu has changed draw it
  if (currentSubMenu != previousSubMenu) {
    switch (currentMenu) {
      case 's':
        maxSubMenu = sSubMenuNum;
        subMenu(maxSubMenu);
        break;
      case 'i':
        maxSubMenu = iSubMenuNum;
        subMenu(maxSubMenu);
        break;
      case 'd':
        maxSubMenu = dSubMenuNum;
        subMenu(maxSubMenu);
        break;
    }
    previousSubMenu = currentSubMenu;
  }

  // get the current date and time from the rtc
  DateTime now = rtc.now();

  if ( ((now.year() != gps.date.year()) || (now.month() != gps.date.month()) || (now.day() != gps.date.day()) ||
        (now.hour() != gps.time.hour()) || (now.minute() != gps.time.minute())) 
      && gps.date.isValid() && gps.time.isValid() &&(gps.date.year() > 2000 ) ) { // how can time/date be valid when its 0 0 2000?
    rtc.adjust(DateTime(gps.date.year(), gps.date.month(), gps.date.day(), gps.time.hour(), gps.time.minute(), gps.time.second()));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
    Serial.println("rtc set from gps");
    displayInfo();
  }

  // if date has changed update it on the bottom display
  if ((now.year() != currentYear) || (now.month() != currentMonth) || (now.day() != currentDay)) {
    currentYear  = now.year();
    currentMonth = now.month();
    currentDay   = now.day();
    displayDate();
    if (SDpresent == 1) startGPX(); // if a Sd is present then start a log
  }

  //if time has changed update it on the bottom display
  if ((now.hour() != currentHour) || (now.minute() != currentMinute)) {
    currentHour      = now.hour();
    currentMinute    = now.minute();
    currentSecond    = now.second();
    currentDayOfWeek = now.dayOfTheWeek();
    displayTime();
  }

  //if location has changed log it to the .gpx
  if (gps.location.isValid() && ((currentLatitude != gps.location.lat()) || (currentLongitude != gps.location.lng()) )) {
    currentLatitude  = gps.location.lat();
    currentLongitude = gps.location.lng();
    if (SDpresent == 1) {
      tft.drawBitmap(0, 0, epd_bitmap_SDWrite, 6, 9, ST77XX_GREEN);
      File gpxdataFile = SD.open(gpxfileName, FILE_WRITE);
      if (gpxdataFile) {
        gpxdataFile.print("   <trkpt lat=\"");
        gpxdataFile.print(currentLatitude, 6);
        gpxdataFile.print("\" lon=\"");
        gpxdataFile.print(currentLongitude, 6);
        gpxdataFile.println("\">");
        gpxdataFile.print("   <time>");
        gpxdataFile.print(currentYear);
        gpxdataFile.print("-");
        if (currentMonth < 10) gpxdataFile.print(F("0"));
        gpxdataFile.print(currentMonth);
        gpxdataFile.print("-");
        if (currentDay < 10) gpxdataFile.print(F("0"));
        gpxdataFile.print(currentDay);
        gpxdataFile.print("T");
        if (currentHour < 10) gpxdataFile.print(F("0"));
        gpxdataFile.print(currentHour);
        gpxdataFile.print(":");
        if (currentMinute < 10) gpxdataFile.print(F("0"));
        gpxdataFile.print(currentMinute);
        gpxdataFile.print(":");
        gpxdataFile.print(now.second(), DEC);
        gpxdataFile.println("Z</time>");
        gpxdataFile.println("   </trkpt>");
        gpxdataFile.close();
      }
      // if the file isn't open, pop up an error:
      else {
        Serial.println("error writing gpx log");
        SDpresent = 0;
        tft.drawBitmap(0, 0, epd_bitmap_SDcard, 6, 9, ST77XX_RED);
      }
    }
    tft.drawBitmap(0, 0, epd_bitmap_SDWrite, 6, 9, ST77XX_BLACK);
    tft.drawBitmap(0, 0, epd_bitmap_SDcard, 6, 9, ST77XX_GREEN);
    //displayInfo();
  }

  // read a character from the GPS into the buffer
  if (Serial1.available() > 0)
    if (gps.encode(Serial1.read())) {
    }
}

bool rotaryEncoder(int8_t &delta) { // https://forum.arduino.cc/t/how-to-use-a-rotary-encoder/678250/2
  delta = 0;
  enum {STATE_LOCKED, STATE_TURN_RIGHT_START, STATE_TURN_RIGHT_MIDDLE, STATE_TURN_RIGHT_END, STATE_TURN_LEFT_START, STATE_TURN_LEFT_MIDDLE, STATE_TURN_LEFT_END, STATE_UNDECIDED};
  static uint8_t encoderState = STATE_LOCKED;
  bool a = !digitalRead(encoderApin);
  bool b = !digitalRead(encoderBpin);
  bool s = !digitalRead(encoderSw);
  static bool switchState = s;
  switch (encoderState) {
    case STATE_LOCKED:
      if (a && b) {
        encoderState = STATE_UNDECIDED;
      }
      else if (!a && b) {
        encoderState = STATE_TURN_LEFT_START;
      }
      else if (a && !b) {
        encoderState = STATE_TURN_RIGHT_START;
      }
      else {
        encoderState = STATE_LOCKED;
      };
      break;
    case STATE_TURN_RIGHT_START:
      if (a && b) {
        encoderState = STATE_TURN_RIGHT_MIDDLE;
      }
      else if (!a && b) {
        encoderState = STATE_TURN_RIGHT_END;
      }
      else if (a && !b) {
        encoderState = STATE_TURN_RIGHT_START;
      }
      else {
        encoderState = STATE_LOCKED;
      };
      break;
    case STATE_TURN_RIGHT_MIDDLE:
    case STATE_TURN_RIGHT_END:
      if (a && b) {
        encoderState = STATE_TURN_RIGHT_MIDDLE;
      }
      else if (!a && b) {
        encoderState = STATE_TURN_RIGHT_END;
      }
      else if (a && !b) {
        encoderState = STATE_TURN_RIGHT_START;
      }
      else {
        encoderState = STATE_LOCKED;
        delta = -1;
      };
      break;
    case STATE_TURN_LEFT_START:
      if (a && b) {
        encoderState = STATE_TURN_LEFT_MIDDLE;
      }
      else if (!a && b) {
        encoderState = STATE_TURN_LEFT_START;
      }
      else if (a && !b) {
        encoderState = STATE_TURN_LEFT_END;
      }
      else {
        encoderState = STATE_LOCKED;
      };
      break;
    case STATE_TURN_LEFT_MIDDLE:
    case STATE_TURN_LEFT_END:
      if (a && b) {
        encoderState = STATE_TURN_LEFT_MIDDLE;
      }
      else if (!a && b) {
        encoderState = STATE_TURN_LEFT_START;
      }
      else if (a && !b) {
        encoderState = STATE_TURN_LEFT_END;
      }
      else {
        encoderState = STATE_LOCKED;
        delta = 1;
      };
      break;
    case STATE_UNDECIDED:
      if (a && b) {
        encoderState = STATE_UNDECIDED;
      }
      else if (!a && b) {
        encoderState = STATE_TURN_RIGHT_END;
      }
      else if (a && !b) {
        encoderState = STATE_TURN_LEFT_END;
      }
      else {
        encoderState = STATE_LOCKED;
      };
      break;
  }

  uint32_t current_time = millis();
  static uint32_t switch_time = 0;
  const uint32_t bounce_time = 30;
  bool back = false;
  if (current_time - switch_time >= bounce_time) {
    if (switchState != s) {
      switch_time = current_time;
      back = s;
      switchState = s;
    }
  }
  return back;
}

void displayInfo()
{
  Serial.print(F("Location: "));
  //if (gps.location.isValid())
  //{
  Serial.print(gps.location.lat(), 6);
  Serial.print(F(","));
  Serial.print(gps.location.lng(), 6);
  //}
  //else
  //{
  //  Serial.print(F("INVALID"));
  //}

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid())
  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.println();
}

void drawmenu(char menu) {
  tft.fillScreen(ST77XX_BLACK);
  tft.drawBitmap(0, 0, epd_bitmap_SDcard, 6, 9, ST77XX_GREEN);
  if (SDpresent == 0) {
    tft.drawBitmap(0, 0, epd_bitmap_SDcard, 6, 9, ST77XX_RED);
    //tft.drawLine(0,0,6,9, ST77XX_RED); // Draw the left menu tab
  }
  tft.drawLine(0, menuHeight, 160, menuHeight, ST77XX_GREEN);
  //  tft.setFont(&Org_01);
  tft.setTextSize(1);              // Normal 1:1 pixel scale
  //  tft.setTextColor(ST77XX_GREEN); // Draw GREEN text
  //  tft.clearDisplay();
  tft.drawLine(0, menuHeight, 128, menuHeight, ST77XX_GREEN);
  int menustart = 8;
  tft.setCursor(menustart, 5);
  if (menu == 's') {
    //tft.drawBitmap(menustart,0,epd_bitmap_ml_squarehalf,5,10, ST77XX_GREEN);
    tft.drawLine(menustart     , menuHeight, menustart + 4 , 0, ST77XX_GREEN); // Draw the left menu tab
    tft.drawLine(menustart + 4 , 0, menustart + 30, 0, ST77XX_GREEN); // Draw the overline
    tft.drawLine(menustart + 30, 0, menustart + 34, menuHeight, ST77XX_GREEN); // then the right side of menu tab
    tft.drawLine(menustart + 1 , menuHeight, menustart + 33, menuHeight, ST77XX_BLACK); // Erase the underline
    tft.setTextColor(ST77XX_GREEN); // Draw GREEN text
    menustart += 6;
    subMenu(3);
  } else
    tft.setTextColor(menuDimColor); // Draw Light green text
  tft.setCursor(menustart, menu_Y_Origin);
  tft.print("STAT");
  menustart += 30;
  if (menu == 's') menustart += 6;
  tft.setCursor(menustart, menu_Y_Origin);

  if (menu == 'i') {
    tft.drawLine(menustart   , menuHeight, menustart + 4 , 0, ST77XX_GREEN); // Draw the left menu tab
    tft.drawLine(menustart + 4 , 0, menustart + 24, 0, ST77XX_GREEN); // Draw the overline
    tft.drawLine(menustart + 24, 0, menustart + 28, menuHeight, ST77XX_GREEN); // then the right side of menu tab
    tft.drawLine(menustart + 1 , menuHeight, menustart + 27, menuHeight, ST77XX_BLACK); // Erase the underline
    tft.setTextColor(ST77XX_GREEN); // Draw GREEN text
    menustart += 6;
    subMenu(4);
  } else
    tft.setTextColor(menuDimColor); // Draw Light green text
  tft.setCursor(menustart, menu_Y_Origin);
  tft.print("INV");
  menustart += 24;
  if (menu == 'i') menustart += 6;
  tft.setCursor(menustart, menu_Y_Origin);

  if (menu == 'd') {
    tft.drawLine(menustart   , menuHeight, menustart + 4 , 0, ST77XX_GREEN); // Draw the left menu tab
    tft.drawLine(menustart + 4 , 0, menustart + 30, 0, ST77XX_GREEN); // Draw the overline
    tft.drawLine(menustart + 30, 0, menustart + 34, menuHeight, ST77XX_GREEN); // then the right side of menu tab
    tft.drawLine(menustart + 1 , menuHeight, menustart + 33, menuHeight, ST77XX_BLACK); // Erase the underline
    tft.setTextColor(ST77XX_GREEN); // Draw GREEN text
    menustart += 6;
    subMenu(3);
    tft.setCursor(12, 22);
    tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK); // Draw GREEN text
    tft.print("Latitude  ");
    tft.print(gps.location.lat(), 6);
    tft.setCursor(12, 30);
    tft.print("Longitude ");
    tft.print(gps.location.lng(), 6);
    tft.setCursor(12, 38);
    tft.print("Year  ");
    tft.print(currentYear);
    tft.setCursor(12, 46);
    tft.print("Month ");
    if (currentMonth < 10) tft.print(F("0"));
    tft.print(currentMonth);
    tft.setCursor(12, 54);
    tft.print("Day   ");
    if (currentDay < 10) tft.print(F("0"));
    tft.print(currentDay);
    tft.setCursor(12, 60);
    tft.print("Hour  ");
    if (currentHour < 10) tft.print(F("0"));
    tft.print(currentHour);
    tft.setCursor(12, 68);
    tft.print("Min   ");
    if (currentMinute < 10) tft.print(F("0"));
    tft.print(currentMinute);

  } else
    tft.setTextColor(menuDimColor); // Draw Light green text
  tft.setCursor(menustart, menu_Y_Origin);
  tft.print("DATA");
  menustart += 30;
  if (menu == 'd') menustart += 6;
  tft.setCursor(menustart, menu_Y_Origin);

  if (menu == 'm') {
    tft.drawLine(menustart   , menuHeight, menustart + 4 , 0, ST77XX_GREEN); // Draw the left menu tab
    tft.drawLine(menustart + 4 , 0, menustart + 24, 0, ST77XX_GREEN); // Draw the overline
    tft.drawLine(menustart + 24, 0, menustart + 28, menuHeight, ST77XX_GREEN); // then the right side of menu tab
    tft.drawLine(menustart + 1 , menuHeight, menustart + 27, menuHeight, ST77XX_BLACK); // Erase the underline
    tft.setTextColor(ST77XX_GREEN); // Draw GREEN text
    menustart += 6;
    subMenu(0);
  } else
    tft.setTextColor(menuDimColor); // Draw Light green text
  tft.setCursor(menustart, menu_Y_Origin);
  tft.print("MAP");
  menustart += 24;
  if (menu == 'm') menustart += 6;
  tft.setCursor(menustart, menu_Y_Origin);

  if (menu == 'r') {
    tft.drawLine(menustart   , menuHeight, menustart + 4 , 0, ST77XX_GREEN); // Draw the left menu tab
    tft.drawLine(menustart + 4 , 0, menustart + 36, 0, ST77XX_GREEN); // Draw the overline
    tft.drawLine(menustart + 36, 0, menustart + 40, menuHeight, ST77XX_GREEN); // then the right side of menu tab
    tft.drawLine(menustart + 1 , menuHeight, menustart + 39, menuHeight, ST77XX_BLACK); // Erase the underline
    tft.setTextColor(ST77XX_GREEN); // Draw GREEN text
    menustart += 6;
  } else
    tft.setTextColor(menuDimColor); // Draw Light green text
  tft.setCursor(menustart, menu_Y_Origin);
  tft.print("RADIO");

  displayDate(); //since erasing the entire screen, need to redraw the bottom
  displayTime();
};

void erase(void){
  tft.setCursor(0, 21);
  for (int j = 0; j < 12; j++) {
    tft.println("                         ");
  }
  tft.setCursor(0, 111);
  tft.println("                         ");
}

void NewsubMenu(int subMenuSize) {
  int submenupos = 0;
  int submenulen = 0;
  switch (currentMenu) {
    case 's':
      submenupos = 15;
      break;
    case 'i':
      submenupos = 45;
      break;
    case 'd':
      submenupos = 68;
      break;
    case 'm':
      submenupos = 80;
      break;
  }
  tft.setCursor(submenupos, 13);
  for (int i = currentSubMenu; i < subMenuSize; i++) {
    if (currentSubMenu == i) tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK); else tft.setTextColor(menuDimColor, ST77XX_BLACK);
    switch (currentMenu) {
      case 's':
        tft.print(sSubMenu[currentSubMenu]);
        break;
      case 'i':
        tft.print(iSubMenu[currentSubMenu]);
        break;
      case 'd':
        tft.print(dSubMenu[currentSubMenu]);
        break;
    }
    tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK);
    tft.print(" ");
  }

  tft.setCursor(submenupos, 20);
  for (int i = currentSubMenu; i < subMenuSize; i++) {
    if (currentSubMenu == i) tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK); else tft.setTextColor(menuDimColor, ST77XX_BLACK);
    switch (currentMenu) {
      case 's':
        tft.print(sSubMenu[currentSubMenu].length() * 5);
        break;
      case 'i':
        tft.print(iSubMenu[currentSubMenu]);
        break;
      case 'd':
        tft.print(dSubMenu[currentSubMenu]);
        break;
    }
    tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK);
    tft.print(" ");
  }
}

void subMenu(int subMenuSize) {
  tft.setCursor(12, 13);
  for (int i = 0; i < subMenuSize; i++) {
    if (currentSubMenu == i) tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK); else tft.setTextColor(menuDimColor, ST77XX_BLACK);
    switch (currentMenu) {
      case 's':
        tft.print(sSubMenu[i]);
        break;
      case 'i':
        tft.print(iSubMenu[i]);
        break;
      case 'd':
        tft.print(dSubMenu[i]);
        break;
    }
    tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK);
    tft.print(" ");
  }
  erase();
  if ((currentMenu == 'i') && (currentSubMenu == 0)) {
    tft.setCursor(12, 22);
    for (int j = 0; j < numWeapons; j++) {
      tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK);
      if ((currentSubMenuItem == j) && (subMenuSelected == 1))  tft.setTextColor(ST77XX_GREEN, menuDimColor); else  tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK);
      tft.setCursor(12, 22+j*9);
      tft.print(invWeapon[j].item);
      tft.setCursor(80, 22+j*9);
      tft.print(invWeapon[j].quantity);
      }
    } //else erase();
  if ((currentMenu == 's') && (currentSubMenu == 0)) {
    #define LOGO_WIDTH    104
    #define LOGO_HEIGHT   100
    tft.drawBitmap(
      (tft.width()  - LOGO_WIDTH ) / 2,
      20,
      epd_bitmap_104x100pb_bw, LOGO_WIDTH, LOGO_HEIGHT, ST77XX_GREEN);
    
    displayWeapon();
    /* moved to separate displayWeapon function
    int starty = 38;
    tft.setCursor(5, starty+4);
    tft.print(F("    "));
    tft.setCursor(5, starty+12);
    tft.print(F("    "));
    tft.setCursor(7, starty+12);
    if (ammoTotal < 10) tft.print(F(" "));
    tft.print(ammoTotal);
    tft.setTextColor(ST77XX_GREEN);
    tft.drawBitmap(6, starty, epd_bitmap_pistol, 25, 24, ST77XX_GREEN);
    */
  }
  if ((currentMenu == 'm') ) {
    int mapWindowXmin = 1;
    int mapWindowXmax = 159;
    int mapWindowYmin = 11;
    int mapWindowYmax = 116;
    
//    tft.setCursor(60, 11);
//    tft.print(maxLat,6);
//    tft.setCursor(0, 60);
//    tft.print(minLon,6);
//    tft.setCursor(100, 60);
//    tft.print(maxLon,6);
//    tft.setCursor(60, 110);
//    tft.print(minLat,6);
//    tft.setCursor(12, 30);
    //tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK); // Draw GREEN text
    //tft.print("Latitude  ");
    //tft.print(gps.location.lat(), 6);
    //tft.setCursor(12, 38);
    //tft.print("Longitude ");
    //tft.print(gps.location.lng(), 6);
    for(int i = 0; i < 4; i++) {
    Serial.println(((maxLon-waypoints[i].lon) * (mapWindowXmax-mapWindowXmin)) / (maxLon - minLon),6);
    Serial.println(((maxLat-waypoints[i].lat) * (mapWindowYmax-mapWindowYmin)) / (maxLat - minLat),6);
    tft.drawPixel(((maxLon-waypoints[i].lon) * (mapWindowXmax-mapWindowXmin)) / (maxLon - minLon) , mapWindowYmin+ ((maxLat-waypoints[i].lat) * (mapWindowYmax-mapWindowYmin)) / (maxLat - minLat), ST77XX_GREEN);
    }


    
    } //else erase();
} 

void displayWeapon(void){
  if (subMenuSelected == 1) tft.setTextColor(ST77XX_GREEN, menuDimColor);
    else tft.setTextColor(ST77XX_GREEN, ST77XX_BLACK);
  int starty = 38;
  tft.setCursor(5, starty+4);
  tft.print(F("    "));
  tft.setCursor(5, starty+12);
  tft.print(F("    "));
  tft.setCursor(7, starty+12);
  if (currentWeapon > 0) {
    if (ammoTotal < 10) tft.print(F(" "));
    tft.print(ammoTotal);
    }
  tft.setTextColor(ST77XX_GREEN);

  Serial.print("current Weapon is ");
  Serial.print(currentWeapon);

  switch (currentWeapon) {
    case 1:
        tft.drawBitmap(6, starty, epd_bitmap_pistol, 25, 24, ST77XX_GREEN);
      break;
    case 2:
        tft.drawBitmap(6, starty, epd_bitmap_revolver, 25, 24, ST77XX_GREEN);
      break;
    default:
      // statements
      break;
  }

  

}




void displayDate(void) {
  tft.setCursor(64, 120);
  //tft.setTextColor(ST77XX_GREEN,ST77XX_BLACK); // Erase old
  //tft.print("            ");
  tft.setCursor(64, 120);
  tft.setTextColor(ST77XX_GREEN, menuDimColor); // Draw new
  if (currentMonth < 10) tft.print(F(" "));
  tft.print(currentMonth);
  tft.print(F("/"));
  uint16_t displayDay;
  if (TimeZonePosNeg == '-') {
    if (currentHour < TimeZoneOffset) {
      displayDay = currentDay - 1;
    } else displayDay = currentDay;
  } else displayDay = currentDay;
  if (displayDay < 10) tft.print(F(" "));
  tft.print(displayDay);
  tft.print(F("/"));
  tft.print(currentYear);
  tft.print(F(" "));
};

void displayTime(void) {
  tft.setCursor(130, 120);
  //tft.setTextColor(ST77XX_GREEN,ST77XX_BLACK); // Erase old
  //tft.print("            ");
  tft.setCursor(130, 120);
  tft.setTextColor(ST77XX_GREEN, menuDimColor); // Draw new time
  uint16_t timeZoneHour = 00;                   // adjust display for time zone
  if (TimeZonePosNeg == '-') {
    if (currentHour < TimeZoneOffset) currentHour = currentHour + 24;
    timeZoneHour = currentHour - TimeZoneOffset;
  } else timeZoneHour = currentHour + TimeZoneOffset;
  if ((TwelveHourTime == 1) && (timeZoneHour > 12)) timeZoneHour = timeZoneHour -12;
  if (timeZoneHour < 10) 
    if (TwelveHourTime == 1) tft.print(F(" "));     // print the time zone adjusted time rather than currentTime
    if (TwelveHourTime == 0) tft.print(F("0"));     // print the time zone adjusted time rather than currentTime
  tft.print(timeZoneHour);
  tft.print(F(":"));
  if (currentMinute < 10) tft.print(F("0"));
  tft.print(currentMinute);
};

void startGPX(void) {
  gpxfileName = String(currentYear);
  if (currentMonth < 10) gpxfileName.concat("0");
  gpxfileName.concat(currentMonth);
  if (currentDay < 10) gpxfileName.concat("0");
  gpxfileName.concat(currentDay);
  gpxfileName.concat(".gpx");
  if (SD.exists(gpxfileName)) {
    // if it already exists then don't write the header again
    Serial.print("Found and using Existing ");
    Serial.println(gpxfileName);
  } else {
    Serial.print("Creating new gpx log ");
    Serial.println(gpxfileName);
    File gpxdataFile = SD.open(gpxfileName, FILE_WRITE);
    if (gpxdataFile) {
      gpxdataFile.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>");
      gpxdataFile.println("<gpx creator=\"pico\">");
      gpxdataFile.println(" <trk>");
      gpxdataFile.print("  <name>");
      gpxdataFile.print(gpxfileName);
      gpxdataFile.println("</name>");
      gpxdataFile.println("  <trkseg>");
      gpxdataFile.close();
    }
    // if the file isn't open, pop up an error:
    else {
      Serial.println("error opening gpx log");
    }
  }
}

int getInv(char fileName[]) {
  File csvFile = SD.open(fileName);

  // find the number of lines in the file
  int numLines = 0;
  while (csvFile.available()) {
    char c = csvFile.read();
    int cIndex = 0;
    while (c != '\n' && cIndex < MAX_STRING_SIZE) {
      cIndex++;
      if (!csvFile.available()) break;
      c = csvFile.read();
    }
    numLines++;
  }  

  // make an array the size of the number of lines
  char pointer[numLines][MAX_STRING_SIZE];
  
  // go back to the beginning of the file 
  csvFile.seek(0);

  // read each line in to the array
  int lineNum = 0;
  char line[MAX_STRING_SIZE];
  while (csvFile.available()) {
    
    char c = csvFile.read();
    int cIndex = 0;
    while (c != '\n' && cIndex < MAX_STRING_SIZE) {
      line[cIndex] = c;
      line[cIndex + 1] = '\0';
      cIndex++;
      if (!csvFile.available()) break;
      c = csvFile.read();
    }
    strcpy(pointer[lineNum], line);
    lineNum++;
  }  
  csvFile.close();

  // read each line from the array and find the separater
  //char separater  = ',';
  int  itemNum=0;
  char item[20]="";
  int  quantity=0;
  for (int p = 0; p < numLines; p++) {
 
    sscanf( pointer[p],"%d,%[^,],%d\n",&itemNum,item, &quantity );
    Serial.print(itemNum);
    Serial.print(" ");
    Serial.print(item);
    Serial.print(" ");
    Serial.println(quantity);

    if (fileName == "Inv/Weapons.csv"){    
      invWeapon[p].itemNum, itemNum;
      strcpy(invWeapon[p].item, item);
      invWeapon[p].quantity = quantity;
    }
    if (fileName == "Inv/Appareal.csv"){
      invApparel[p].itemNum, itemNum;
      strcpy(invApparel[p].item, item);
      invApparel[p].quantity = quantity;
    }
    if (fileName == "Inv/Aid.csv"){
      invAid[p].itemNum, itemNum;
      strcpy(invAid[p].item, item);
      invAid[p].quantity = quantity;
    }
    if (fileName == "Inv/Misc.csv"){
      invMisc[p].itemNum, itemNum;
      strcpy(invMisc[p].item, item);
      invMisc[p].quantity = quantity;
    }
    if (fileName == "Inv/Ammo.csv"){
      invAmmo[p].itemNum, itemNum;
      strcpy(invAmmo[p].item, item);
      invAmmo[p].quantity = quantity;
    }

    /*
    //Serial.println(pointer[p]);
    char *position_ptr = strchr(pointer[p],separater);
    int  separaterPos = position_ptr - pointer[p];
    char *endposition_ptr = strchr(pointer[p],'\0');
    int  len = endposition_ptr - pointer[p];
    char item[MAX_STRING_SIZE];
    for (int t = 0; t < separaterPos; t++) {
      item[t] = pointer[p][t];
      item[t+1] = '\0';
    }
 //   Serial.println(item);

    if (fileName == "Inv/Weapons.csv") strcpy(invWeapon[p].item, item);
    if (fileName == "Inv/Appareal.csv") strcpy(invApparel[p].item, item);
    if (fileName == "Inv/Aid.csv") strcpy(invAid[p].item, item);
    if (fileName == "Inv/Misc.csv") strcpy(invMisc[p].item, item);
    if (fileName == "Inv/Ammo.csv") strcpy(invAmmo[p].item, item);
    
    char quantity[MAX_STRING_SIZE];
    for (int q = 0; q < len-separaterPos; q++) {
      quantity[q] = pointer[p][q+separaterPos+1];
      quantity[q+1] = '\0';  
    }
 //   Serial.println(quantity);
    if (fileName == "Inv/Weapons.csv") invWeapon[p].quantity = strtoul(quantity, NULL, 10);
    if (fileName == "Inv/Apparel.csv") invApparel[p].quantity = strtoul(quantity, NULL, 10);
    if (fileName == "Inv/Aid.csv") invAid[p].quantity = strtoul(quantity, NULL, 10);
    if (fileName == "Inv/Misc.csv") invMisc[p].quantity = strtoul(quantity, NULL, 10);
    if (fileName == "Inv/Ammo.csv") invAmmo[p].quantity = strtoul(quantity, NULL, 10);
  */
  
  }
  

  return numLines;
}

void getMap(void)
{
  File csvFile = SD.open("map/wp.gpx");
//  Serial.println("map/wp.gpx");
  // find the number of lines in the file
  int numLines = 0;
  while (csvFile.available()) {
    char c = csvFile.read();
    int cIndex = 0;
    while (c != '\n' && cIndex < MAX_STRING_SIZE) {
      cIndex++;
      if (!csvFile.available()) break;
      c = csvFile.read();
    }
    numLines++;
   }  
  // Serial.println(numLines);

  // make an array the size of the number of lines
  char waypoint[numLines][MAX_STRING_SIZE];
  
  // go back to the beginning of the file 
  csvFile.seek(0);

  int  lineNum = 0;
  int  wptsindex = 0;
  int  wpteindex = 0;
  char ch      = '\0';
  int  index    = 0;
  while (csvFile.available()) {
    ch = csvFile.read();
    waypoint[lineNum][index] = ch;
    waypoint[lineNum][index+1] = '\0';
    index++;
    if (wptsindex == 4) {
      wptsindex ++;
      wpteindex = 0;
    }
    if (wptsindex >= 4) {
      //Serial.print(ch);
      wptsindex ++;
    }
    if (wpteindex == 6) {
      wpteindex++;
      wptsindex = 0;
      index = 0;
      lineNum++;
    }
    
  if (ch == '<' && wptsindex == 0) wptsindex = 1;
  if (ch == 'w' && wptsindex == 1) wptsindex = 2;
  if (ch == 'p' && wptsindex == 2) wptsindex = 3;
  if (ch == 't' && wptsindex == 3) wptsindex = 4;

  if (index > (MAX_STRING_SIZE - 2)) index = 0; // failsafe

  if (ch == '<' && wpteindex == 0) wpteindex = 1;
  if (ch == '/' && wpteindex == 1) wpteindex = 2;
  if (ch == 'w' && wpteindex == 2) wpteindex = 3;
  if (ch == 'p' && wpteindex == 3) wpteindex = 4;
  if (ch == 't' && wpteindex == 4) wpteindex = 5;
  if (ch == '>' && wpteindex == 5) wpteindex = 6;
  
  }

  for(int i = 0; i < lineNum; i++) {
//    Serial.print(i);
//    Serial.print(" : ");
    //Serial.println(waypoint[i]);
    String wpt = waypoint[i];
//    Serial.println(wpt);
    String wptname = wpt.substring(wpt.indexOf("<name>")+6,wpt.indexOf("</name>"));
//    Serial.println(wptname);
    String latstr = wpt.substring(wpt.indexOf("lat=\"")+5,wpt.indexOf("lon=\""));
//    Serial.println(latstr);
    double lat = latstr.toDouble();
//    Serial.println(lat, 6);
    String lonstr = wpt.substring(wpt.indexOf("lon=\"")+5,wpt.indexOf("<name>"));
//    Serial.println(lonstr);
    double lon = lonstr.toDouble();
//    Serial.println(lon, 6);
    waypoints[i].name = wptname;
    waypoints[i].lat = lat;
    waypoints[i].lon = lon;
  }

  maxLat = waypoints[0].lat;
  minLat = waypoints[0].lat;
  maxLon = waypoints[0].lon;
  minLon = waypoints[0].lon;
  for(int i = 0; i < lineNum; i++) {
    Serial.print(waypoints[i].name);
    Serial.print(" ");
    Serial.print(waypoints[i].lat,6);
    Serial.print(" ");
    Serial.println(waypoints[i].lon,6);
    maxLat = max(waypoints[i].lat,maxLat);
    minLat = min(waypoints[i].lat,minLat);
    maxLon = max(waypoints[i].lon,maxLon);
    minLon = min(waypoints[i].lon,minLon);
  }
   Serial.println(maxLat,6);
   Serial.println(minLat,6);
   Serial.println(maxLon,6);
   Serial.println(minLon,6);
}
