bool     SDpresent;        // Track if the microSD is present or not

void setupSD(){
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("SD card failed, or not present");
    tft.println("SD card failed, or not present");
    SDpresent = 0;
    // don't do anything more:
    //while (1);
  } else {
    Serial.println("SD card initialized.");
    tft.println("SD card initialized.");
    SDpresent = 1;
  }
}
