//                 GPIO   pico pin
//      Serial1 Tx   0 //  1
//      Serial1 Rx   1 //  2 From GPS
//      Ground             3
//                   2 //  4
//                   3 //  5
//      I2C SDA      4 //  6
//      I2C SCL      5 //  7
//      Ground             8
#define switch1      6 //  9 // main menu switch 1
#define switch2      7 // 10 // main menu switch 2
//                   8 // 11
//                   9 // 12
//      Ground            13
//                  10 // 14
//                  11 // 15
//                  12 // 16
#define encoderSw   13 // 17
//      Ground            18
#define encoderApin 14 // 19
#define encoderBpin 15 // 20

//      MISO        16 // 21 // SD MISO
#define chipSelect  17 // 22 // SD CS
//      Ground            23
//      SCLK        18 // 24 // SD and TFT SCLK
//      MOSI        19 // 25 // SD and TFT MOSI
#define TFT_DC      20 // 26 // moved for breadboard ease from 8  
#define TFT_RST     21 // 27 // moved for breadboard ease from 9   Or set to -1 and connect to Arduino RESET pin
//      Ground            28
#define TFT_CS      22 // 29 // moved for breadboard ease from 10 
//      Run/Reset      // 30
//                  26 // 31
//                  27 // 32
//      Ground            33
#define ADC2        28 // 34
//                     // 35
//                     // 36
//                     // 37
//                     // 38
//                     // 39
//                     // 40
