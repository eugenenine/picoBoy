// very simple commands
// test patterns
//  cd("inv");
//  Serial.println(cwd);
//  dir();
//  cd("\0");
//  Serial.println(cwd);
//  cd("inv/");
//  Serial.println(cwd);
//  cd("/");
//  Serial.println(cwd);
//  dir();
//  cd("/inv");
//  Serial.println(cwd);
//  dir();
//  cd("/inv/");
//  Serial.println(cwd);
//  dir();

void prompt(void){
  Serial.print(cwd); Serial.print(">");
}

//cd : provides a Change Directory command and checks for existance of and if the name is a valid directory
char * cd(char cd[]){
  File file;
  if (cd[0] == '\0') {// print current working directory
    Serial.println(cwd);
  } else
  if ((strlen(cd) == 1) && (cd[0] == '/')) {// go back to root
     cwd[0] = '/';
     cwd[1] = '\0';
  }
  else {// appeand to path
    //Serial.println(strcpy(cwd,cd));
    char newwd[100] = "\0"; // initial curent working directory  
    if (cd[0] == '/') strcpy(newwd,cd); 
    else {
      strcpy(newwd,cwd);
      strcat(newwd,cd);
      }
    if (newwd[strlen(newwd)-1] != '/') strcat(newwd,"/");
    //Serial.print("new working directory will be ");
    //Serial.println(newwd);
    if (SD.exists(newwd)) { 
      file = SD.open(newwd);
        if (file.isDirectory()) {
          //Serial.println(" is a directory");
          strcpy(cwd,newwd);
        } else Serial.println(" is a file not a directory");
     } else Serial.println(" does not exist");
  }
  //return cwd;
}

//dir : provides a Directory listing command
char *  dir(char args[]) {
  int fileCount = 0;
  File dir;
  dir = SD.open(cwd);
    while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    fileCount++;
    //Serial.println(entry.name());
    entry.close();
  }
  //char dirarray[fileCount][14];
  //Serial.println(fileCount);
  fileCount = 0;
  char* dirarray[20]; //i used maximum of 20 elements to be stored
  dir = SD.open(cwd);
    while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
      char* temp = entry.name();
      String toString = String(temp); //converted filename to String

      toString.toCharArray(temp, toString.length()+1); //converted it back to char array for it to be stored in char* plantList
      dirarray[fileCount]=strdup(temp);

      fileCount++;
  }
  
  //Serial.println(fileCount);
  //for (int x = 0; x < fileCount; x++)
  //{
   // if (dirarray[x] != NULL)  //only print levels where data exists
    //{
    //  Serial.print(x);
    //  Serial.print("\t");
    //  Serial.println(dirarray[x]);
   // }
  //}
  for(int i = 0; i < fileCount; i++)
  {
    Serial.println(dirarray[i]);
  }
  //return * dirarray;
}

//date : To display or set the system date or time.
//in the format Monday 25-Nov-24 18:40:47
char * date(char args[]){
  if (args[0] == '\0') {// no parameter displays the currently set system date and time.
    Serial.print(daysOfTheWeek[currentDayOfWeek]);
    Serial.print(F(" "));
    Serial.print(currentDay);
    Serial.print(F("-"));
    Serial.print(monthOfTheYear[currentMonth-1]);
    Serial.print(F("-"));
    Serial.print(currentYear);
    Serial.print(F(" "));
    if (currentHour < 10) Serial.print(F("0"));
    Serial.print(currentHour);
    Serial.print(F(":"));
    if (currentMinute < 10) Serial.print(F("0"));
    Serial.print(currentMinute);
    Serial.print(F(":"));
    if (currentSecond < 10) Serial.print(F("0"));
    Serial.println(currentSecond);
  } //else
  if (strcmp("tomorrow", args) == 0){
    rtc.adjust(DateTime(currentYear, currentMonth, currentDay+1, currentHour, currentMinute, currentSecond));
  }
  if (strcmp("yesterday", args) == 0){
    rtc.adjust(DateTime(currentYear, currentMonth, currentDay-1, currentHour, currentMinute, currentSecond));
  }
}

void echo(char args[]){
  Serial.println(args);
}

void makedir(char args[]){
  char newd[100] = "\0"; // new path/directory
  // need to check for 8 char
  // check if already esists
  strcpy(newd,cwd);
  strcat(newd,args);
  if (SD.mkdir(newd))
    Serial.println("Created directory");
  else
    Serial.println("Failed to create directory");
}


char processCharInput(char* cliBuffer, const char c)
{
  //https://forum.arduino.cc/t/read-and-compare-string-form-serial-port/100028/3
  //Store the character in the input buffer
  if (c >= 32 && c <= 126) //Ignore control characters and special ascii characters
  {
    if (strlen(cliBuffer) < CMDBUFFER_SIZE) 
    { 
      strncat(cliBuffer, &c, 1);   //Add it to the buffer
    }
    else  
    {   
      return '\n';
    }
  }
  else if ((c == 8 || c == 127) && cliBuffer[0] != 0) //Backspace
  {
    cliBuffer[strlen(cliBuffer)-1] = 0;
  }
  return c;
}
