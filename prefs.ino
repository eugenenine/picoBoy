
void getPrefs(void)
{
  File csvFile = SD.open("Prefs/Menu");

  // find the number of lines in the file
  int numLines = 0;
  while (csvFile.available()) {
    char c = csvFile.read();
    int cIndex = 0;
    while (c != '\n' && cIndex < MAX_STRING_SIZE) {
      cIndex++;
      if (!csvFile.available()) break;
      c = csvFile.read();
    }
    numLines++;
  }  
  //Serial.println(numLines);

  // make an array the size of the number of lines
  char pointer[numLines][MAX_STRING_SIZE];
  
  // go back to the beginning of the file 
  csvFile.seek(0);

  // read each line in to the array
  int lineNum = 0;
  char line[MAX_STRING_SIZE];
  while (csvFile.available()) {
    
    char c = csvFile.read();
    int cIndex = 0;
    while (c != '\n' && cIndex < MAX_STRING_SIZE) {
      line[cIndex] = c;
      line[cIndex + 1] = '\0';
      cIndex++;
      if (!csvFile.available()) break;
      c = csvFile.read();
    }
    //Serial.print(lineNum);
    //Serial.print(" ");
    //Serial.println(line);
    strcpy(pointer[lineNum], line);
    lineNum++;
  }  
  csvFile.close();

  // read each line from the array and find the separater
  char comma  = '=';
  for (int p = 0; p < numLines; p++) {
    //Serial.println(pointer[p]);
    char *position_ptr = strchr(pointer[p],comma);
    int commaPos = position_ptr - pointer[p];
    char*endposition_ptr = strchr(pointer[p],'\0');
    int len = endposition_ptr - pointer[p];
    //Serial.print("comma position " );
    //Serial.print(commaPos );
    //Serial.print(" length " );
    //Serial.println(len );
    char pref[MAX_STRING_SIZE];
    for (int t = 0; t < commaPos; t++) {
      pref[t] = pointer[p][t];
      pref[t+1] = '\0';
    }
    char setting[MAX_STRING_SIZE];
    for (int s = 0; s < len-commaPos; s++) {
      setting[s] = pointer[p][s+commaPos+1];
      setting[s+1] = '\0';
    } 
    
  // set the time zone 
    if(strcmp(pref, "TimeZone") == 0) {
      TimeZonePosNeg = setting[0];
     // Serial.println(TimeZonePosNeg);
     // Serial.println(setting[1]);
     // Serial.println(setting[2]);
      if (setting[2] == '\0') {
        TimeZoneOffset = abs(atoi(setting));
      }
     // Serial.println(TimeZoneOffset);
    }
  // set twelve or 24 hour time display  
    if(strcmp(pref, "TwelveHourTime") == 0) {
      TwelveHourTime = setting[0];
    //  Serial.println(TwelveHourTime);
    }
  // menuColor 
    if(strcmp(pref, "menuColor") == 0) {
      //Serial.println(setting);
      menuColor = strtoul(setting, NULL, 16);
    //  Serial.println(menuColor);
    }   
  // menuDimColor
    if(strcmp(pref, "menuDimColor") == 0) {
      menuDimColor = strtoul(setting, NULL, 16);
   //   Serial.println(menuDimColor);
    }
  }
}



    /*  

  String radioPrefString = "";
  if (SD.exists(radioPrefs)) {
    Serial.println("Radio preferences found");
    File radioPrefFile = SD.open(radioPrefs);
    while (radioPrefFile.available())
    {
      radioPrefString += (char)radioPrefFile.read();
    }
    radioPrefFile.close();
  };

  */
